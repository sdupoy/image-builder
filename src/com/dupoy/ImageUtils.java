package com.dupoy;

import com.dupoy.config.Config;
import com.dupoy.data.actions.Action;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.List;

public class ImageUtils
{
    public BufferedImage scaleImage(BufferedImage inputImage)
    {
        int max = Config.maxInputImageDimension;
        int inputWidth = inputImage.getWidth();
        int inputHeight = inputImage.getHeight();
        if (inputWidth <= max && inputHeight <= max)
        {
            // no need to scale
            return inputImage;
        }

        // get dimensions to scale to
        int scaledWidth;
        int scaledHeight;
        if (inputWidth > inputHeight)
        {
            scaledWidth = max;
            scaledHeight = (int)(((double)max / (double)inputWidth) * (double)inputHeight);
        }
        else
        {
            scaledWidth = (int)(((double)max / (double)inputHeight) * (double)inputWidth);
            scaledHeight = max;
        }

        // scale the image
        System.out.println("scaling input image from (" + inputWidth + " x " + inputHeight + ") to (" + scaledWidth + " x " + scaledHeight + ")");
        BufferedImage scaledImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = scaledImage.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        graphics.dispose();
        return scaledImage;
    }

    public BufferedImage getFinalImage(int width, int height, Color backgroundColor, List<Action> actions, double scale)
    {
        BufferedImage image = this.createInitialImage(width, height, backgroundColor);
        Graphics2D graphics = image.createGraphics();
        graphics.scale(scale, scale);
        for (Action action : actions)
        {
            action.render(graphics);
        }
        graphics.dispose();
        return image;
    }

    public BufferedImage createInitialImage(int width, int height, Color backgroundColor)
    {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = image.createGraphics();
        graphics.setColor(backgroundColor);
        graphics.fillRect(0, 0, width, height);
        graphics.dispose();
        return image;
    }

    public BufferedImage cloneImageAndApplyAction(BufferedImage sourceImage, Action action)
    {
        BufferedImage clonedImage = this.cloneImage(sourceImage);
        Graphics2D graphics = clonedImage.createGraphics();
        action.render(graphics);
        graphics.dispose();
        return clonedImage;
    }

    private BufferedImage cloneImage(BufferedImage sourceImage)
    {
        BufferedImage newImage = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), sourceImage.getType());
        Graphics2D graphics = newImage.createGraphics();
        graphics.drawImage(sourceImage, 0, 0, null);
        graphics.dispose();
        return newImage;
    }
}
