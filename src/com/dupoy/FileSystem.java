package com.dupoy;

import com.dupoy.config.Config;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class FileSystem
{
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");

    public BufferedImage loadImage()
            throws IOException
    {
        System.out.println("Reading input image: " + Config.inputFilePath);
        return ImageIO.read(new File(Config.inputFilePath));
    }

    public void saveProgressImage(BufferedImage image, int iteration)
            throws IOException
    {
        // get the progress file path
        String iterationString = Integer.toString(iteration);
        while (iterationString.length() < 4)
        {
            iterationString = "0" + iterationString;
        }
        String progressFilePath = String.format(Config.progressFilePathTemplate, this.getTimestampString(), iterationString);

        System.out.println("Writing progress file: " + progressFilePath);
        ImageIO.write(image, Config.outputFileType, new File(progressFilePath));
    }

    public void saveFinalImage(BufferedImage image)
            throws IOException
    {
        String outputFilePath = String.format(Config.outputFilePathTemplate, this.getTimestampString());
        System.out.println("Writing output file: " + outputFilePath);
        ImageIO.write(image, Config.outputFileType, new File(outputFilePath));
    }

    private String getTimestampString()
    {
        return Config.timestamp.format(dateTimeFormatter);
    }
}
