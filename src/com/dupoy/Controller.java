package com.dupoy;

import com.dupoy.config.Config;
import com.dupoy.data.State;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.CandidateAction;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Controller
{
    private FileSystem fileSystem = new FileSystem();
    private ColorAverager colorAverager = new ColorAverager();
    private ImageUtils imageUtils = new ImageUtils();
    private ImageComparer imageComparer = new ImageComparer();
    private InitialActionFinder initialActionFinder = new InitialActionFinder();
    private HillClimber hillClimber = new HillClimber();

    public void run()
            throws IOException
    {
        System.out.println("Running image builder");
        long start = System.currentTimeMillis();

        State state = this.getInitialState();
        List<Action> actions = new ArrayList<>();
        double scale = (double)state.originalWidth / (double)state.targetImage.getWidth();

        for (int iteration = 1; iteration <= Config.iterations; iteration++)
        {
            System.out.println("iteration " + iteration + " / " + Config.iterations);

            // get as best an action as we can
            CandidateAction action;
            action = this.initialActionFinder.getInitialAction(state);
            action = this.hillClimber.optimize(state, action);
            actions.add(action.action);

            // set the current image to be the best action found
            state.currentImage = action.image;
            state.currentDifference = action.difference;

            // save progress if necessary
            BufferedImage progressImage = state.currentImage;
            if (iteration % Config.saveProgressInterval == 0)
            {
                if (Config.saveProgressIntervalFullQuality)
                {
                    progressImage = this.imageUtils.getFinalImage(state.originalWidth, state.originalHeight, state.backgroundColor, actions, scale);
                }
                this.fileSystem.saveProgressImage(progressImage, iteration);
            }
        }

        // build and save the final image
        BufferedImage finalImage = this.imageUtils.getFinalImage(state.originalWidth, state.originalHeight, state.backgroundColor, actions, scale);
        this.fileSystem.saveFinalImage(finalImage);

        long duration = (System.currentTimeMillis() - start) / 1000;
        System.out.println("Finished in " + duration + " seconds");
    }

    private State getInitialState()
            throws IOException
    {
        State state = new State();
        BufferedImage inputImage = this.fileSystem.loadImage();

        state.originalWidth = inputImage.getWidth();
        state.originalHeight = inputImage.getHeight();
        state.backgroundColor = this.colorAverager.getAverage(inputImage);

        state.targetImage = this.imageUtils.scaleImage(inputImage);
        state.currentImage = this.imageUtils.createInitialImage(state.targetImage.getWidth(), state.targetImage.getHeight(), state.backgroundColor);

        //state.currentDifference = this.imageComparer.compareFullImage(state.targetImage, state.currentImage);
        state.currentDifference = this.imageComparer.compare(state.targetImage, state.currentImage);
        return state;
    }
}
