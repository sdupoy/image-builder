package com.dupoy;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ColorAverager
{
    public Color getAverage(BufferedImage image)
    {
        int[] rgbs = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        int n = rgbs.length;
        long red = 0;
        long green = 0;
        long blue = 0;
        for (int i = 0; i < n; i++)
        {
            Color inputPixelColor = new Color(rgbs[i]);
            red += inputPixelColor.getRed();
            green += inputPixelColor.getGreen();
            blue += inputPixelColor.getRed();
        }
        red /= n;
        green /= n;
        blue /= n;
        return new Color((int)red, (int)green, (int)blue);
    }

    public Color getAverage(BufferedImage image, int x, int y, int w, int h)
    {
        long red = 0;
        long green = 0;
        long blue = 0;
        int count = 0;
        for (int i = x; i < (x + w); i++)
        {
            for (int j = y; j < (y + h); j++)
            {
                Color inputPixelColor = new Color(image.getRGB(i, j));
                int r = inputPixelColor.getRed();
                int g = inputPixelColor.getGreen();
                int b = inputPixelColor.getBlue();

                //System.out.println("(" + i + ", " + j + ") => r: " + r + ", g: " + g + ", b: " + b);
                red += r;
                green += g;
                blue += b;
                count++;
            }
        }
        //System.out.println("count: " + count);
        if (count == 0)
        {
            return new Color(0, 0, 0);
        }
        red /= count;
        green /= count;
        blue /= count;
        return new Color((int)red, (int)green, (int)blue);
    }
}
