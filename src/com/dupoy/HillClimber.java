package com.dupoy;

import com.dupoy.data.Delta;
import com.dupoy.data.State;
import com.dupoy.data.actions.CandidateAction;
import com.dupoy.data.actions.ModifiedAction;
import com.dupoy.data.shapes.types.Polygon;
import com.dupoy.mutations.ActionMutator;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class HillClimber
{
    private ImageComparer imageComparer = new ImageComparer();
    private ImageUtils imageUtils = new ImageUtils();
    private ActionMutator actionMutator = new ActionMutator();

    // position, size, color, rotation
    private List<Delta> deltas = Arrays.asList(
            new Delta(48, 30, 20, 15),
            new Delta(24, 18, 10, 10),
            new Delta(12, 12, 5, 5),
            new Delta(6, 6, 3, 2),
            new Delta(1, 1, 1, 1)
    );

    public CandidateAction optimize(State state, CandidateAction candidateAction)
    {
        System.out.println("hill climbing");
        int imageWidth = state.targetImage.getWidth();
        int imageHeight = state.targetImage.getHeight();

        for (Delta delta : this.deltas)
        {
            // keep on looking for improvements to the action at this delta until we can't find any more
            System.out.println(
                    "  hill climb delta: position: " + delta.positionDelta +
                    ", size: " + delta.sizeDelta +
                    ", color: " + delta.colorDelta +
                    ", rotationInDegrees: " + delta.rotationDeltaInDegrees);

            while (true)
            {
                // iterate over each of the modified actions calculate the image and difference (in parallel)
                List<ModifiedAction> modifiedActions = this.actionMutator.getMutations(candidateAction.action, delta, imageWidth, imageHeight);
                modifiedActions.parallelStream().forEach((modifiedAction) ->
                {
                    modifiedAction.modifiedActionImage = this.imageUtils.cloneImageAndApplyAction(state.currentImage, modifiedAction.action);
                    modifiedAction.modifiedActionDifference = this.imageComparer.compare(state.targetImage, modifiedAction.modifiedActionImage);
                });

                // now iterate over the list of modified actions and see which is the best
                ModifiedAction bestModifiedAction = null;
                for (ModifiedAction modifiedAction : modifiedActions)
                {
                    // if the modified action is no better than the candidate action one then move on
                    if (modifiedAction.modifiedActionDifference >= candidateAction.difference)
                    {
                        // this modified action does not improve things
                        continue;
                    }

                    // this modified action improves things
                    candidateAction.difference = modifiedAction.modifiedActionDifference;
                    candidateAction.image = modifiedAction.modifiedActionImage;
                    candidateAction.action = modifiedAction.action;
                    bestModifiedAction = modifiedAction;
                }

                // iterated over them all, were there any improvements?
                if (bestModifiedAction != null)
                {
                    System.out.println("    new best difference: " + String.format("%.2f", candidateAction.difference) + ", action type: " + bestModifiedAction.description);
                }
                else
                {
                    // no improvements so break out of the while loop and decrease the delta
                    break;
                }
            }

            // exited while loop so no further improvements at this delta. continue for loop to decrease the delta.
        }

        // now finished with the smallest delta level
        return candidateAction;
    }
}
