package com.dupoy;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageComparer
{
    public long compare(BufferedImage image1, BufferedImage image2)
    {
        // assuming dimensions are the same
        long difference = 0;
        int width = image1.getWidth();
        int height = image1.getHeight();
        int[] image1Rgb = image1.getRGB(0, 0,width, height, null, 0, width);
        int[] image2Rgb = image2.getRGB(0, 0,width, height, null, 0, width);
        int n = image1Rgb.length;
        for (int i = 0; i < n; i++)
        {
            Color color1 = new Color(image1Rgb[i]);
            Color color2 = new Color(image2Rgb[i]);
            int redDiff = color1.getRed() - color2.getRed();
            int greenDiff = color1.getGreen() - color2.getGreen();
            int blueDiff = color1.getBlue() - color2.getBlue();
            difference += (redDiff * redDiff) + (greenDiff * greenDiff) + (blueDiff * blueDiff); // square diffs
        }
        // don't see why we need the sqrt or the /n... we're just interested in minimising the differences, not
        // the quantified difference value.
        //return Math.sqrt((double)difference / n);
        return difference;
    }
}
