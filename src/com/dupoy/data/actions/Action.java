package com.dupoy.data.actions;

import com.dupoy.data.shapes.Shape;

import java.awt.Color;
import java.awt.Graphics2D;

public class Action
{
    public Action()
    {
    }

    public Action(Color color, Shape shape)
    {
        this.color = color;
        this.shape = shape;
    }

    public Color color;

    public Shape shape;

    public void render(Graphics2D graphics)
    {
        graphics.setColor(this.color);
        this.shape.render(graphics);
    }
}
