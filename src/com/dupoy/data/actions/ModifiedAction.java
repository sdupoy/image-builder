package com.dupoy.data.actions;

import java.awt.image.BufferedImage;

public class ModifiedAction
{
    public ModifiedAction(Action action, String description)
    {
        this.action = action;
        this.description = description;
    }

    public Action action;

    public String description;

    public BufferedImage modifiedActionImage;

    public double modifiedActionDifference;
}
