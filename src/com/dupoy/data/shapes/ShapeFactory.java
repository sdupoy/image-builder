package com.dupoy.data.shapes;

import com.dupoy.config.Config;
import com.dupoy.data.shapes.types.Circle;
import com.dupoy.data.shapes.types.Ellipse;
import com.dupoy.data.shapes.types.Polygon;
import com.dupoy.data.shapes.types.RotatingEllipse;

public class ShapeFactory
{
    public Shape CreateRandomShape(int imageWidth, int imageHeight)
    {
        switch (Config.Shape)
        {
            case "Ellipse":
                return Ellipse.createRandom(imageWidth, imageHeight);

            case "RotatingEllipse":
                return RotatingEllipse.createRandom(imageWidth, imageHeight);

            case "Triangle":
                return Polygon.createRandom(3, imageWidth, imageHeight);

            case "Circle":
                return Circle.createRandom(imageWidth, imageHeight, false);

            case "FixedDiameterCircle":
                return Circle.createRandom(imageWidth, imageHeight, true);

            default:
                throw new RuntimeException("Unrecognised shape type: " + Config.Shape);
        }
    }
}
