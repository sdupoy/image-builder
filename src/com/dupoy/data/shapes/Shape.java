package com.dupoy.data.shapes;

import com.dupoy.data.Delta;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.ModifiedAction;

import java.awt.*;
import java.util.List;

public abstract class Shape
{
    public abstract Rectangle getBounds();

    public abstract List<ModifiedAction> getMutations(Action action, Delta delta, int imageWidth, int imageHeight);

    public abstract void render(Graphics2D graphics);
}
