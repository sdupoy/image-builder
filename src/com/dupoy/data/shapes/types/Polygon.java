package com.dupoy.data.shapes.types;

import com.dupoy.config.Config;
import com.dupoy.data.Delta;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.ModifiedAction;
import com.dupoy.data.shapes.Shape;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class Polygon extends Shape
{
    private static ThreadLocalRandom random = ThreadLocalRandom.current();

    public List<Point> points = new ArrayList<>();

    // using static constructor only
    private Polygon(List<Point> points)
    {
        // take our own copy of the list of points
        for (Point point : points)
        {
            this.points.add(new Point(point.x, point.y));
        }
    }

    public Rectangle getBounds()
    {
        return getGraphicsPolygon().getBounds();
    }

    @Override
    public void render(Graphics2D graphics)
    {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.fill(this.getGraphicsPolygon());
    }

    @Override
    public java.util.List<ModifiedAction> getMutations(Action action, Delta delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> mutations = new ArrayList<>();
        for (int i = 0; i < this.points.size(); i++)
        {
            for (Point mutatedPoint : this.getMutatedPoints(this.points.get(i), delta.sizeDelta, imageWidth, imageHeight))
            {
                List<Point> mutationPoints = new ArrayList<>();
                String description = "";
                for (int j = 0; j < this.points.size(); j++)
                {
                    Point originalPoint = this.points.get(j);
                    if (i == j)
                    {
                        mutationPoints.add(mutatedPoint);
                        description = "moved vertex " + i + " from (" + originalPoint.x + ", " + originalPoint.y + ") to (" + mutatedPoint.x + ", " + mutatedPoint.y + ")";
                    }
                    else
                    {
                        mutationPoints.add(new Point(originalPoint.x, originalPoint.y));
                    }
                }
                mutations.add(new ModifiedAction(new Action(action.color, new Polygon(mutationPoints)), description));
            }
        }
        return mutations;
    }

    private List<Point> getMutatedPoints(Point point, int delta, int imageWidth, int imageHeight)
    {
        List<Point> mutatedPoints = new ArrayList<>();

        Rectangle bounds = this.getBounds();

        int minX = -(int)(Config.maxProportionOfShapeOffImage * (double)bounds.width);
        int maxX = imageWidth + (int)(Config.maxProportionOfShapeOffImage * (double)bounds.width);

        int reducedX = Math.max(point.x - delta, minX);
        if (reducedX != point.x)
        {
            mutatedPoints.add(new Point(reducedX, point.y));
        }

        int increasedX = Math.min(point.x + delta, maxX);
        if (increasedX != point.x)
        {
            mutatedPoints.add(new Point(increasedX, point.y));
        }

        int minY = -(int)(Config.maxProportionOfShapeOffImage * (double)bounds.height);
        int maxY = imageHeight + (int)(Config.maxProportionOfShapeOffImage * (double)bounds.height);

        int reducedY = Math.max(point.y - delta, minY);
        if (reducedY != point.y)
        {
            mutatedPoints.add(new Point(point.x, reducedY));
        }

        int increasedY = Math.min(point.y + delta, maxY);
        if (increasedY != point.x)
        {
            mutatedPoints.add(new Point(point.x, increasedY));
        }

        return mutatedPoints;
    }

    public static com.dupoy.data.shapes.Shape createRandom(int vertexCount, int imageWidth, int imageHeight)
    {
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < vertexCount; i++)
        {
            points.add(new Point(
                    (int)(random.nextDouble(-imageWidth * 0.25, imageWidth * 1.25)),
                    (int)(random.nextDouble(-imageHeight * 0.25, imageHeight * 1.25))
            ));
        }
        return new Polygon(points);
    }

    private java.awt.Polygon getGraphicsPolygon()
    {
        java.awt.Polygon polygon = new java.awt.Polygon();
        for (Point point : this.points)
        {
            polygon.addPoint(point.x, point.y);
        }
        return polygon;
    }
}
