package com.dupoy.data.shapes.types;

import com.dupoy.config.Config;
import com.dupoy.data.Delta;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.ModifiedAction;
import com.dupoy.data.shapes.Shape;

import java.awt.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Circle extends Shape
{
    protected static ThreadLocalRandom random = ThreadLocalRandom.current();

    private int x;
    private int y;
    private int diameter;
    private boolean fixedDiameter;

    // using static constructor only
    protected Circle(int x, int y, int diameter, boolean fixedDiameter)
    {
        this.x = x;
        this.y = y;
        this.diameter = diameter;
        this.fixedDiameter = fixedDiameter;
    }

    public Rectangle getBounds()
    {
        Rectangle rectangle = new Rectangle();
        rectangle.setRect(this.x, this.y, this.diameter, this.diameter);
        return rectangle;
    }

    @Override
    public void render(Graphics2D graphics)
    {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.fillOval(this.x, this.y, this.diameter, this.diameter);
    }

    @Override
    public java.util.List<ModifiedAction> getMutations(Action action, Delta delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();
        actions.addAll(this.getXModifiedActions(action, delta.positionDelta, imageWidth, imageHeight));
        actions.addAll(this.getYModifiedActions(action, delta.positionDelta, imageWidth, imageHeight));
        if (!this.fixedDiameter)
        {
            actions.addAll(this.getDiameterModifiedActions(action, delta.sizeDelta, imageWidth, imageHeight));
        }
        return actions;
    }

    public static com.dupoy.data.shapes.Shape createRandom(int imageWidth, int imageHeight, boolean fixedDiameter)
    {
        // get a diameter
        int diameter = Config.fixedCircleDiameter;
        if (!fixedDiameter)
        {
            int minimumImageDimension = Math.min(imageWidth, imageHeight);
            diameter = random.nextInt(Config.minShapeDimension, minimumImageDimension);
        }

        // now randomise the position
        int x;
        int y;
        x = random.nextInt(-diameter / 2, imageWidth - (diameter / 2));
        y = random.nextInt(-diameter / 2, imageHeight - (diameter / 2));

        return new Circle(x, y, diameter, fixedDiameter);
    }

    private java.util.List<ModifiedAction> getXModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minX = -(int)(Config.maxProportionOfShapeOffImage * this.diameter);
        int maxX = imageWidth - (int)((1.0 - Config.maxProportionOfShapeOffImage) * this.diameter);

        int reducedX = Math.max(minX, this.x - delta);
        if (reducedX != this.x)
        {
            com.dupoy.data.shapes.Shape newShape = new Circle(reducedX, this.y, this.diameter, this.fixedDiameter);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced x to " + reducedX));
        }

        int increasedX = Math.min(maxX, this.x + delta);
        if (increasedX != this.x)
        {
            com.dupoy.data.shapes.Shape newShape = new Circle(increasedX, this.y, this.diameter, this.fixedDiameter);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased x to " + increasedX));
        }

        return actions;
    }

    private java.util.List<ModifiedAction> getYModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minY = -(int)(Config.maxProportionOfShapeOffImage * this.diameter);
        int maxY = imageHeight - (int)((1.0 - Config.maxProportionOfShapeOffImage) * this.diameter);

        int reducedY = Math.max(minY, this.y - delta);
        if (reducedY != this.y)
        {
            com.dupoy.data.shapes.Shape newShape = new Circle(this.x, reducedY, this.diameter, this.fixedDiameter);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced y to " + reducedY));
        }

        int increasedY = Math.min(maxY, this.y + delta);
        if (increasedY != this.y)
        {
            com.dupoy.data.shapes.Shape newShape = new Circle(this.x, increasedY, this.diameter, this.fixedDiameter);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased y to " + increasedY));
        }

        return actions;
    }

    protected java.util.List<ModifiedAction> getDiameterModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minWidth = Config.minShapeDimension;
        int maxWidth = imageWidth * 2;

        int reducedDiameter = Math.max(minWidth, this.diameter - delta);
        if (reducedDiameter != this.diameter)
        {
            com.dupoy.data.shapes.Shape newShape = new Circle(this.x, this.y, reducedDiameter, this.fixedDiameter);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced diameter to " + reducedDiameter));
        }

        int increasedDiameter = Math.min(maxWidth, this.diameter + delta);
        if (increasedDiameter != this.diameter)
        {
            com.dupoy.data.shapes.Shape newShape = new Circle(this.x, this.y, increasedDiameter, this.fixedDiameter);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased diameter to " + increasedDiameter));
        }

        return actions;
    }
}
