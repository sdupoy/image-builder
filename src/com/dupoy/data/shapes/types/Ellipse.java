package com.dupoy.data.shapes.types;

import com.dupoy.config.Config;
import com.dupoy.data.Delta;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.ModifiedAction;
import com.dupoy.data.shapes.Shape;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Ellipse extends Shape
{
    private static ThreadLocalRandom random = ThreadLocalRandom.current();

    private int x;
    private int y;
    private int width;
    private int height;

    // using static constructor only
    private Ellipse(int x, int y, int width, int height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public Rectangle getBounds()
    {
        Rectangle rectangle = new Rectangle();
        rectangle.setRect(this.x, this.y, this.width, this.height);
        return rectangle;
    }

    @Override
    public void render(Graphics2D graphics)
    {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.fillOval(this.x, this.y, this.width, this.height);
    }

    @Override
    public List<ModifiedAction> getMutations(Action action, Delta delta, int imageWidth, int imageHeight)
    {
        List<ModifiedAction> actions = new ArrayList<>();
        actions.addAll(this.getXModifiedActions(action, delta.positionDelta, imageWidth, imageHeight));
        actions.addAll(this.getYModifiedActions(action, delta.positionDelta, imageWidth, imageHeight));
        actions.addAll(this.getWidthModifiedActions(action, delta.sizeDelta, imageWidth, imageHeight));
        actions.addAll(this.getHeightModifiedActions(action, delta.sizeDelta, imageWidth, imageHeight));
        return actions;
    }

    public static Shape createRandom(int imageWidth, int imageHeight)
    {
        // shape should have a ratio first, some where between, say, 1-4 and 4-1
        double minimumDimension = Config.minShapeDimension;
        double ratio = random.nextDouble(1.0 / Config.maxInitialEllipseRatio, Config.maxInitialEllipseRatio);

        int width;
        int height;
        if (ratio > 1)
        {
            // sideways
            width = (int)random.nextDouble(minimumDimension, imageWidth);
            height = (int)((double)width / ratio);
        }
        else
        {
            // upright
            height = (int) random.nextDouble(minimumDimension, imageHeight);
            width = (int)((double)height * ratio);
        }

        // now randomise the position
        int x;
        int y;
        x = random.nextInt(-width / 2, imageWidth - (width / 2));
        y = random.nextInt(-height / 2, imageHeight - (height / 2));

        return new Ellipse(x, y, width, height);
    }

    private List<ModifiedAction> getXModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minX = -(int)(Config.maxProportionOfShapeOffImage * this.width);
        int maxX = imageWidth - (int)((1.0 - Config.maxProportionOfShapeOffImage) * this.width);

        int reducedX = Math.max(minX, this.x - delta);
        if (reducedX != this.x)
        {
            Shape newShape = new Ellipse(reducedX, this.y, this.width, this.height);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced x to " + reducedX));
        }

        int increasedX = Math.min(maxX, this.x + delta);
        if (increasedX != this.x)
        {
            Shape newShape = new Ellipse(increasedX, this.y, this.width, this.height);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased x to " + increasedX));
        }

        return actions;
    }

    private List<ModifiedAction> getYModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minY = -(int)(Config.maxProportionOfShapeOffImage * this.height);
        int maxY = imageHeight - (int)((1.0 - Config.maxProportionOfShapeOffImage) * this.height);

        int reducedY = Math.max(minY, this.y - delta);
        if (reducedY != this.y)
        {
            Shape newShape = new Ellipse(this.x, reducedY, this.width, this.height);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced y to " + reducedY));
        }

        int increasedY = Math.min(maxY, this.y + delta);
        if (increasedY != this.y)
        {
            Shape newShape = new Ellipse(this.x, increasedY, this.width, this.height);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased y to " + increasedY));
        }

        return actions;
    }

    private List<ModifiedAction> getWidthModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minWidth = Config.minShapeDimension;
        int maxWidth = imageWidth * 2;

        int reducedWidth = Math.max(minWidth, this.width - delta);
        if (reducedWidth != this.width)
        {
            Shape newShape = new Ellipse(this.x, this.y, reducedWidth, this.height);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced width to " + reducedWidth));
        }

        int increasedWidth = Math.min(maxWidth, this.width + delta);
        if (increasedWidth != this.width)
        {
            Shape newShape = new Ellipse(this.x, this.y, increasedWidth, this.height);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased width to " + increasedWidth));
        }

        return actions;
    }

    private List<ModifiedAction> getHeightModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minHeight = Config.minShapeDimension;
        int maxHeight = imageHeight * 2;

        int reducedHeight = Math.max(minHeight, this.height - delta);
        if (reducedHeight != this.height)
        {
            Shape newShape = new Ellipse(this.x, this.y, this.width, reducedHeight);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced height to " + reducedHeight));
        }

        int increasedHeight = Math.min(maxHeight, this.height + delta);
        if (increasedHeight != this.height)
        {
            Shape newShape = new Ellipse(this.x, this.y, this.width, increasedHeight);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased height to " + increasedHeight));
        }

        return actions;
    }
}
