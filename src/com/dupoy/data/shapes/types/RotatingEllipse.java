package com.dupoy.data.shapes.types;

import com.dupoy.config.Config;
import com.dupoy.data.Delta;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.ModifiedAction;
import com.dupoy.data.shapes.Shape;

import java.awt.*;
import java.awt.List;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class RotatingEllipse extends Shape
{
    private static ThreadLocalRandom random = ThreadLocalRandom.current();

    private int centeredX;
    private int centeredY;
    private int ellipseWidth;
    private int ellipseHeight;
    private int angleInDegrees;

    // using static constructor only
    private RotatingEllipse(int centeredX, int centeredY, int ellipseWidth, int ellipseHeight, int angleInDegrees)
    {
        this.centeredX = centeredX;
        this.centeredY = centeredY;
        this.ellipseWidth = ellipseWidth;
        this.ellipseHeight = ellipseHeight;
        this.angleInDegrees = angleInDegrees;
    }

    public Rectangle getBounds()
    {
        // find bounding rectangle
        Rectangle rectangle = new Rectangle();
        BoundingBoxDimensions bounds = getBoundingBoxDimensions();
        rectangle.setFrameFromCenter(this.centeredX, this.centeredY, this.centeredX + bounds.width / 2, this.centeredY + bounds.height / 2);
        return rectangle;
    }

    @Override
    public void render(Graphics2D graphics)
    {
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Ellipse2D ellipse = new Ellipse2D.Float();
        ellipse.setFrameFromCenter(this.centeredX, this.centeredY, this.centeredX + this.ellipseWidth / 2, this.centeredY + this.ellipseHeight / 2);

        // cache the unrotated graphics transform state then rotate
        AffineTransform originalTransform = graphics.getTransform();
        graphics.rotate(Math.toRadians(this.angleInDegrees));

        // draw
        graphics.fill(ellipse);

        // reverse the rotation
        graphics.setTransform(originalTransform);
    }

    @Override
    public java.util.List<ModifiedAction> getMutations(Action action, Delta delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();
        actions.addAll(this.getXModifiedActions(action, delta.positionDelta, imageWidth, imageHeight));
        actions.addAll(this.getYModifiedActions(action, delta.positionDelta, imageWidth, imageHeight));
        actions.addAll(this.getWidthModifiedActions(action, delta.sizeDelta, imageWidth, imageHeight));
        actions.addAll(this.getHeightModifiedActions(action, delta.sizeDelta, imageWidth, imageHeight));
        actions.addAll(this.getAngleModifiedActions(action, delta.rotationDeltaInDegrees, imageWidth, imageHeight));
        return actions;
    }

    public static Shape createRandom(int imageWidth, int imageHeight)
    {
        // shape should have a ratio first, some where between, say, 1-4 and 4-1
        double ratio = random.nextDouble(1.0 / Config.maxInitialEllipseRatio, Config.maxInitialEllipseRatio);

        int ellipseWidth;
        int ellipseHeight;
        if (ratio > 1)
        {
            // sideways
            ellipseWidth = (int)random.nextDouble(Config.minShapeDimension, imageWidth);
            ellipseHeight = (int)((double)ellipseWidth / ratio);
        }
        else
        {
            // upright
            ellipseHeight = (int) random.nextDouble(Config.minShapeDimension, imageHeight);
            ellipseWidth = (int)((double)ellipseHeight * ratio);
        }

        // random angle between 0-180 (pretty sure this could be 0-90 with the -ve to +ve ratio above)
        int angleInDegrees = random.nextInt(180);

        // get the bounding box and randomise the position
        int centeredX = random.nextInt(0, imageWidth);
        int centeredY = random.nextInt(0, imageHeight);

        return new RotatingEllipse(centeredX, centeredY, ellipseWidth, ellipseHeight, angleInDegrees);
    }

    private double getMaxCenteredProportionOfShapeOffImage()
    {
        // amount allowed off the image (therefore a +ve number is probably what we want)
        return Config.maxProportionOfShapeOffImage - 0.5;
    }

    private java.util.List<ModifiedAction> getXModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        BoundingBoxDimensions bounds = getBoundingBoxDimensions();
        int minCenteredX = -(int)(this.getMaxCenteredProportionOfShapeOffImage() * bounds.width);
        int maxCenteredX = imageWidth + (int)(this.getMaxCenteredProportionOfShapeOffImage() * bounds.width);

        int reducedX = Math.max(minCenteredX, this.centeredX - delta);
        if (reducedX != this.centeredX)
        {
            Shape newShape = new RotatingEllipse(reducedX, this.centeredY, this.ellipseWidth, this.ellipseHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced centered x to " + reducedX));
        }

        int increasedX = Math.min(maxCenteredX, this.centeredX + delta);
        if (increasedX != this.centeredX)
        {
            Shape newShape = new RotatingEllipse(increasedX, this.centeredY, this.ellipseWidth, this.ellipseHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased centered x to " + increasedX));
        }

        return actions;
    }

    private java.util.List<ModifiedAction> getYModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        BoundingBoxDimensions bounds = getBoundingBoxDimensions();
        int minCenteredY = -(int)(this.getMaxCenteredProportionOfShapeOffImage() * bounds.height);
        int maxCenteredY = imageHeight + (int)(this.getMaxCenteredProportionOfShapeOffImage() * bounds.height);

        int reducedY = Math.max(minCenteredY, this.centeredY - delta);
        if (reducedY != this.centeredY)
        {
            Shape newShape = new RotatingEllipse(this.centeredX, reducedY, this.ellipseWidth, this.ellipseHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced centered y to " + reducedY));
        }

        int increasedY = Math.min(maxCenteredY, this.centeredY + delta);
        if (increasedY != this.centeredY)
        {
            Shape newShape = new RotatingEllipse(this.centeredX, increasedY, this.ellipseWidth, this.ellipseHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased centered y to " + increasedY));
        }

        return actions;
    }

    private java.util.List<ModifiedAction> getWidthModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minWidth = Config.minShapeDimension;
        int maxWidth = imageWidth * 2;

        int reducedWidth = Math.max(minWidth, this.ellipseWidth - delta);
        if (reducedWidth != this.ellipseWidth)
        {
            Shape newShape = new RotatingEllipse(this.centeredX, this.centeredY, reducedWidth, this.ellipseHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced width to " + reducedWidth));
        }

        int increasedWidth = Math.min(maxWidth, this.ellipseWidth + delta);
        if (increasedWidth != this.ellipseWidth)
        {
            Shape newShape = new RotatingEllipse(this.centeredX, this.centeredY, increasedWidth, this.ellipseHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased width to " + increasedWidth));
        }

        return actions;
    }

    private java.util.List<ModifiedAction> getHeightModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        // work out the range of possible values
        int minHeight = Config.minShapeDimension;
        int maxHeight = imageHeight * 2;

        int reducedHeight = Math.max(minHeight, this.ellipseHeight - delta);
        if (reducedHeight != this.ellipseHeight)
        {
            Shape newShape = new RotatingEllipse(this.centeredX, this.centeredY, this.ellipseWidth, reducedHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "reduced height to " + reducedHeight));
        }

        int increasedHeight = Math.min(maxHeight, this.ellipseHeight + delta);
        if (increasedHeight != this.ellipseHeight)
        {
            Shape newShape = new RotatingEllipse(this.centeredX, this.centeredY, this.ellipseWidth, increasedHeight, this.angleInDegrees);
            actions.add(new ModifiedAction(new Action(action.color, newShape), "increased height to " + increasedHeight));
        }

        return actions;
    }

    private java.util.List<ModifiedAction> getAngleModifiedActions(Action action, int delta, int imageWidth, int imageHeight)
    {
        java.util.List<ModifiedAction> actions = new ArrayList<>();

        int increasedAngle = this.angleInDegrees + delta;
        actions.add(
                new ModifiedAction(
                        new Action(
                            action.color,
                            new RotatingEllipse(this.centeredX, this.centeredY, this.ellipseWidth, this.ellipseHeight, increasedAngle)),
                        "increased rotation to " + increasedAngle + " degrees"));

        int decreasedAngle = this.angleInDegrees - delta;
        actions.add(
                new ModifiedAction(
                        new Action(
                                action.color,
                                new RotatingEllipse(this.centeredX, this.centeredY, this.ellipseWidth, this.ellipseHeight, decreasedAngle)),
                        "decreased rotation to " + decreasedAngle + " degrees"));

        return actions;
    }

    private BoundingBoxDimensions getBoundingBoxDimensions()
    {
        // get the bounding box dimensions for the rotated ellipse
        double diagonalFromCenter = Math.sqrt((double)(this.ellipseWidth * this.ellipseWidth + this.ellipseHeight * this.ellipseHeight)) / 2.0;
        double internalAngleRadians = Math.atan((double)this.ellipseWidth / (double)this.ellipseHeight);

        int simpleAngle = this.angleInDegrees;
        simpleAngle = simpleAngle % 180;
        if (simpleAngle > 90)
        {
            simpleAngle = 90 - (simpleAngle % 90);
        }
        double rotationalAngleInRadians = Math.toRadians(simpleAngle);
        double widthAngleInRadians = internalAngleRadians + rotationalAngleInRadians;
        double heightAngleInRadians = ((Math.PI / 2.0) - internalAngleRadians) + rotationalAngleInRadians;

        BoundingBoxDimensions bounds = new BoundingBoxDimensions();
        bounds.width = (int)Math.abs(2.0 * diagonalFromCenter * Math.sin(widthAngleInRadians));
        bounds.height = (int)Math.abs(2.0 * diagonalFromCenter * Math.sin(heightAngleInRadians));
        return bounds;
    }

    private class BoundingBoxDimensions
    {
        int width;

        int height;
    }
}
