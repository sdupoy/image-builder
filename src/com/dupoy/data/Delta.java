package com.dupoy.data;

public class Delta
{
    public Delta(int positionDelta, int sizeDelta, int colorDelta, int rotationDeltaInDegrees)
    {
        this.positionDelta = positionDelta;
        this.sizeDelta = sizeDelta;
        this.colorDelta = colorDelta;
        this.rotationDeltaInDegrees = rotationDeltaInDegrees;
    }

    public int positionDelta;

    public int sizeDelta;

    public int colorDelta;

    public int rotationDeltaInDegrees;
}
