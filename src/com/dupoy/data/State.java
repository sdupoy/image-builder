package com.dupoy.data;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class State
{
    public int originalWidth;

    public int originalHeight;

    public Color backgroundColor;

    public BufferedImage targetImage;

    public BufferedImage currentImage;

    public double currentDifference;
}
