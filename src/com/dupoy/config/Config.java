package com.dupoy.config;

import java.time.LocalDateTime;

public class Config
{
    public static LocalDateTime timestamp = LocalDateTime.now();

    public static String workingFolder = "/Users/scott/Pictures/_Ovals/";

    public static String fileName = "aitutaki";

    public static String inputFilePath = workingFolder + fileName + "/" + fileName + ".jpg";

    public static String outputFileType = "png";

    public static String outputFilePathTemplate = workingFolder + fileName + "/" + fileName + "-%s-final." + outputFileType;

    public static String progressFilePathTemplate = workingFolder + fileName + "/" + fileName + "-%s-progress-%s." + outputFileType;

    // RotatingEllipse => lots of thin ellipses - need to work on that!
    public static String Shape = "Ellipse";

    public static int maxInputImageDimension = 256;

    public static int iterations = 1200;

    public static int initialShapeIterations = 60;

    public static int saveProgressInterval = 50;

    public static boolean saveProgressIntervalFullQuality = true;

    public static double maxInitialEllipseRatio = 4.0;

    public static int defaultColorAlphaLevel = 128;
    public static int minAlphaLevel = 108;
    public static int maxAlphaLevel = 148;

    // experiments with fixed size circles
    //public static int defaultColorAlphaLevel = 255;
    //public static int minAlphaLevel = 220;
    //public static int maxAlphaLevel = 255;

    public static int minShapeDimension = 2;

    public static int fixedCircleDiameter = 8;

    public static double maxProportionOfShapeOffImage = 0.8;
}
