package com.dupoy;

import com.dupoy.config.Config;
import com.dupoy.data.*;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.CandidateAction;
import com.dupoy.data.shapes.ShapeFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class InitialActionFinder
{
    private ShapeFactory shapeFactory = new ShapeFactory();
    private ColorAverager colorAverager = new ColorAverager();
    private ImageComparer imageComparer = new ImageComparer();
    private ImageUtils imageUtils = new ImageUtils();

    public CandidateAction getInitialAction(State state)
    {
        System.out.println("getting initial action");

        // create a structure of containers in an array that we can then process in parallel
        List<ActionIteration> actionIterations = new ArrayList<>();
        for (int iteration = 1; iteration <= Config.initialShapeIterations; iteration++)
        {
            actionIterations.add(new ActionIteration(iteration));
        }

        // now process these iterations in parallel
        actionIterations.parallelStream().forEach(actionIteration ->
        {
            actionIteration.action = this.getRandomAction(state.targetImage);
            actionIteration.image = this.imageUtils.cloneImageAndApplyAction(state.currentImage, actionIteration.action);
            actionIteration.difference = this.imageComparer.compare(state.targetImage, actionIteration.image);
        });

        // find the best iteration
        CandidateAction bestCandidateAction = new CandidateAction();
        bestCandidateAction.difference = state.currentDifference; // need to better this
        for (ActionIteration actionIteration : actionIterations)
        {
            if (actionIteration.difference < bestCandidateAction.difference)
            {
                bestCandidateAction.difference = actionIteration.difference;
                bestCandidateAction.action = actionIteration.action;
                bestCandidateAction.image = actionIteration.image;
                System.out.println("  new best candidate initial difference: " + String.format("%.2f", bestCandidateAction.difference) + " on iteration " + actionIteration.iteration);
            }
        }

        // an extra while loop just in case we couldn't find an initial iteration. not parallelising this one as it's an edge case.
        int totalIterations = Config.initialShapeIterations;
        while (bestCandidateAction.action == null)
        {
            // clone the current image, get a random action, apply it, see if it's the best initial image
            totalIterations++;
            Action candidateAction = this.getRandomAction(state.targetImage);
            BufferedImage candidateImage = this.imageUtils.cloneImageAndApplyAction(state.currentImage, candidateAction);
            double candidateDifference = this.imageComparer.compare(state.targetImage, candidateImage);
            if (candidateDifference < bestCandidateAction.difference)
            {
                bestCandidateAction.difference = candidateDifference;
                bestCandidateAction.action = candidateAction;
                bestCandidateAction.image = candidateImage;
                System.out.println("  new best candidate initial difference: " + String.format("%.2f", bestCandidateAction.difference) + " on extra iteration " + totalIterations);
            }
        }
        System.out.println("  found initial action after " + totalIterations + " iterations");

        return bestCandidateAction;
    }

    private Action getRandomAction(BufferedImage targetImage)
    {
        // need to scale this up to the biggest it could potentially be
        Action action = new Action();
        action.shape = this.shapeFactory.CreateRandomShape(targetImage.getWidth(), targetImage.getHeight());

        // now need to find the average target colour (just doing it in the rectangle bounded by the image, note it can be out of bounds)
        Rectangle bounds = action.shape.getBounds();
        int x = bounds.x;
        int y = bounds.y;
        int w = bounds.width;
        int h = bounds.height;

        // bounds check left
        if (x < 0)
        {
            w += x; // double negative
            x = 0;
        }

        // bound check top
        if (y < 0)
        {
            h += y; // double negative
            y = 0;
        }

        // bounds right
        if (x + w > targetImage.getWidth())
        {
            w = targetImage.getWidth() - x;
        }

        // bounds check bottom
        if (y + h > targetImage.getHeight())
        {
            h = targetImage.getHeight() - y;
        }

        // get the pixels in the rectangle and get their average colour for the ellipse starting colour (better than random!)
        // SCOTT: will this favour a smaller initial image?
        action.color = this.colorAverager.getAverage(targetImage, x, y, w, h);
        action.color = new Color(action.color.getRed(), action.color.getGreen(), action.color.getBlue(), Config.defaultColorAlphaLevel);
        return action;
    }

    private class ActionIteration
    {
        ActionIteration(int iteration)
        {
            this.iteration = iteration;
        }

        int iteration;

        Action action;

        BufferedImage image;

        double difference;
    }
}
