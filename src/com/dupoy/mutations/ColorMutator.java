package com.dupoy.mutations;

import com.dupoy.config.Config;
import com.dupoy.data.Delta;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.ModifiedAction;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ColorMutator
{
    public List<ModifiedAction> getColorMutations(Action action, Delta delta)
    {
        List<ModifiedAction> actions = new ArrayList<>();
        actions.addAll(this.getRedModifiedActions(action, delta.colorDelta));
        actions.addAll(this.getGreenModifiedActions(action, delta.colorDelta));
        actions.addAll(this.getBlueModifiedActions(action, delta.colorDelta));
        actions.addAll(this.getAlphaModifiedActions(action, delta.colorDelta));
        return actions;
    }

    private List<ModifiedAction> getRedModifiedActions(Action action, int delta)
    {
        List<ModifiedAction> actions = new ArrayList<>();
        int red = action.color.getRed();

        int reducedRed = Math.max(0, red - delta);
        if (reducedRed != red)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(reducedRed, action.color.getGreen(), action.color.getBlue(), action.color.getAlpha()),
                    action.shape),
                    "reduced red to " + reducedRed));
        }

        int increasedRed = Math.min(255, red + delta);
        if (increasedRed != red)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(increasedRed, action.color.getGreen(), action.color.getBlue(), action.color.getAlpha()),
                    action.shape),
                    "increased red to " + increasedRed));
        }

        return actions;
    }

    private List<ModifiedAction> getGreenModifiedActions(Action action, int delta)
    {
        List<ModifiedAction> actions = new ArrayList<>();
        int green = action.color.getGreen();

        int reducedGreen = Math.max(0, green - delta);
        if (reducedGreen != green)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(action.color.getRed(), reducedGreen, action.color.getBlue(), action.color.getAlpha()),
                    action.shape),
                    "reduced green to " + reducedGreen));
        }

        int increasedGreen = Math.min(255, green + delta);
        if (increasedGreen != green)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(action.color.getRed(), increasedGreen, action.color.getBlue(), action.color.getAlpha()),
                    action.shape),
                    "increased green to " + increasedGreen));
        }

        return actions;
    }

    private List<ModifiedAction> getBlueModifiedActions(Action action, int delta)
    {
        List<ModifiedAction> actions = new ArrayList<>();
        int blue = action.color.getBlue();

        int reducedBlue = Math.max(0, blue - delta);
        if (reducedBlue != blue)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(action.color.getRed(), action.color.getGreen(), reducedBlue, action.color.getAlpha()),
                    action.shape),
                    "reduced blue to " + reducedBlue));
        }

        int increasedBlue = Math.min(255, blue + delta);
        if (increasedBlue != blue)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(action.color.getRed(), action.color.getGreen(), increasedBlue, action.color.getAlpha()),
                    action.shape),
                    "increased blue to " + increasedBlue));
        }

        return actions;
    }

    private List<ModifiedAction> getAlphaModifiedActions(Action action, int delta)
    {
        List<ModifiedAction> actions = new ArrayList<>();
        int alpha = action.color.getAlpha();

        // note that the alpha level has mix/max levels
        int reducedAlpha = Math.max(Config.minAlphaLevel, alpha - delta);
        if (reducedAlpha != alpha)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(action.color.getRed(), action.color.getGreen(), action.color.getBlue(), reducedAlpha),
                    action.shape),
                    "reduced alpha to " + reducedAlpha));
        }

        int increasedAlpha = Math.min(Config.maxAlphaLevel, alpha + delta);
        if (increasedAlpha != alpha)
        {
            actions.add(new ModifiedAction(new Action(
                    new Color(action.color.getRed(), action.color.getGreen(), action.color.getBlue(), increasedAlpha),
                    action.shape),
                    "increased alpha to " + increasedAlpha));
        }

        return actions;
    }
}
