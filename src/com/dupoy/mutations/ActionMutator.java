package com.dupoy.mutations;

import com.dupoy.data.Delta;
import com.dupoy.data.actions.Action;
import com.dupoy.data.actions.ModifiedAction;

import java.util.ArrayList;
import java.util.List;

public class ActionMutator
{
    private ColorMutator colorMutator = new ColorMutator();

    public List<ModifiedAction> getMutations(Action action, Delta delta, int imageWidth, int imageHeight)
    {
        // these methods could be improved in terms of traversing ridges as
        // each mutation only progresses in one "direction" at a time
        List<ModifiedAction> actions = new ArrayList<>();
        actions.addAll(this.colorMutator.getColorMutations(action, delta));
        actions.addAll(action.shape.getMutations(action, delta, imageWidth, imageHeight));
        return actions;
    }
}
